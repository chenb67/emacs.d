(setq inhibit-startup-message t)
(setq comp-speed 2)

(require 'package)
(setq package-enable-at-startup nil)

(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/") t)

(add-to-list 'package-archives
             '("melpa-stable" . "https://stable.melpa.org/packages/") t)

(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)

(add-to-list 'load-path
	     "~/.emacs.d/lisp/")

(package-initialize)

;; Bootstrap `use-package'
(unless (package-installed-p 'use-package)
	(package-refresh-contents)
	(package-install 'use-package))

;; (defvar bootstrap-version)
;; (let ((bootstrap-file
;;        (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
;;       (bootstrap-version 5))
;;   (unless (file-exists-p bootstrap-file)
;;     (with-current-buffer
;;         (url-retrieve-synchronously
;;          "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
;;          'silent 'inhibit-cookies)
;;       (goto-char (point-max))
;;       (eval-print-last-sexp)))
;;   (load bootstrap-file nil 'nomessage))

(setq package-enable-at-startup nil)

(setq
;; use-package-always-defer t
 use-package-always-ensure t
 backup-directory-alist `((".*" . ,temporary-file-directory))
 auto-save-file-name-transforms `((".*" ,temporary-file-directory t)))

;; Package installation and configuration

;; (use-package auto-package-update
;;   :config
;;   (setq auto-package-update-delete-old-versions t)
;;   (setq auto-package-update-hide-results t)
;;   (auto-package-update-at-time "12:30"))

(use-package company)

(add-hook 'after-init-hook 'global-company-mode)

(when (memq window-system '(mac ns x))
  (exec-path-from-shell-initialize))

;; define exec-path
(exec-path-from-shell-copy-env "PATH")
(exec-path-from-shell-copy-env "NVM_BIN")
(exec-path-from-shell-copy-env "NVM_CD_FLAGS")
(exec-path-from-shell-copy-env "NVM_DIR")
;; (require 'doremi)

(use-package try
  :defer t)

(use-package which-key
  :config
  (which-key-mode))

(use-package vterm
  :ensure t
  :config
  (add-hook 'vterm-mode-hook
  (lambda()
    (local-unset-key (kbd "M-<"))
    (local-unset-key (kbd "M->"))
    ))
  )

(use-package doom-modeline
      :ensure t
      :hook (after-init . doom-modeline-mode))

(use-package org
  :ensure org
  :pin org
  :config
  (unbind-key "<S-right>" org-mode-map)
  (unbind-key "<S-left>" org-mode-map)
  (unbind-key "<S-up>" org-mode-map)
  (unbind-key "<S-down>" org-mode-map)
  (unbind-key "C-c C-<" org-mode-map)
  (setq org-image-actual-width nil)
  (setq org-default-notes-file (concat org-directory "/notes.org"))
  (define-key global-map "\C-cc" 'org-capture)
  (setq org-modules (quote (org-protocol)))
  
  (require 'ob-shell)
  (org-babel-do-load-languages 'org-babel-load-languages
			       '((shell . t))
			       )
  (setq org-capture-templates
      '(("t" "Todo" entry (file+headline "~/org/globaltodo.org" "Tasks")
         "* TODO %?\n  %i\n")
        ("j" "Journal" entry (file+datetree "~/org/journal.org")
         "* %?\nEntered on %U\n  %i\n  %a")
	("i" "Idea" entry (file+datetree "~/org/ideas.org")
         "* %?\nEntered on %U\n  %i\n  %a")
	("d" "DontSleepWithout" entry (file+datetree "~/org/dsw.org")
         "* %?\nEntered on %U\n  %i\n  %a")))

;;    (require 'org-expiry)
  
  ;; Configure it a bit to my liking
  (setq
   org-expiry-created-property-name "CREATED" ; Name of property when an item is created
   org-expiry-inactive-timestamps   t         ; Don't have everything in the agenda view
   org-agenda-window-setup 'other-window
   )

  (defun mrb/insert-created-timestamp()
    "Insert a CREATED property using org-expiry.el for TODO entries"
    (org-expiry-insert-created)
    (org-back-to-heading)
    (org-end-of-line)
    (insert " ")
    )

  ;; Whenever a TODO entry is created, I want a timestamp
  ;; Advice org-insert-todo-heading to insert a created timestamp using org-expiry
  (defadvice org-insert-todo-heading (after mrb/created-timestamp-advice activate)
    "Insert a CREATED property using org-expiry.el for TODO entries"
    (mrb/insert-created-timestamp)
    )
  ;; Make it active
  (ad-activate 'org-insert-todo-heading)

  )

  
;; (use-package org-roam
;;       :ensure t
;;       :hook
;;       (after-init . org-roam-mode)
;;       :custom
;;       (org-roam-directory "/Users/yakir/gdrive/org/")
;;       :bind (:map org-roam-mode-map
;;               (("C-c n l" . org-roam)
;;                ("C-c n f" . org-roam-find-file)
;;                ("C-c n g" . org-roam-graph))
;;               :map org-mode-map
;;               (("C-c n i" . org-roam-insert))
;;               (("C-c n I" . org-roam-insert-immediate))))

(use-package org-bullets
  :after org
  :config
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))

(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "multimarkdown"))

(use-package all-the-icons)

(use-package powerline)

(use-package doom-themes
  :config
  (require 'doom-themes)

  ;; Global settings (defaults)
  (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
	doom-themes-enable-italic t) ; if nil, italics is universally disabled

  ;; Load the theme (doom-one, doom-molokai, etc); keep in mind that each theme
  ;; may have their own settings.
  (load-theme 'doom-one t)

  ;; Enable flashing mode-line on errors
  (doom-themes-visual-bell-config)

  ;; Enable custom neotree theme (all-the-icons must be installed!)
  ;; (doom-themes-neotree-config)
  ;; or for treemacs users
  (doom-themes-treemacs-config)

  ;; Corrects (and improves) org-mode's native fontification.
  (doom-themes-org-config))

(use-package rainbow-delimiters
  :config
  (add-hook 'prog-mode-hook #'rainbow-delimiters-mode))



(use-package centaur-tabs
  :demand
  :config
  (centaur-tabs-mode t)
  (setq centaur-tabs-set-icons t)
  :bind
  ("C-<prior>" . centaur-tabs-backward)
  ("C-<next>" . centaur-tabs-forward))

(use-package exec-path-from-shell
  :commands exec-path-from-shell-copy-env)

(use-package magit
  :bind ("C-x g" . magit-status))

(use-package avy
  :bind
  ("M-g SPC" . avy-goto-char-timer)
  ("M-g f" . avy-goto-line))

(use-package flycheck
  :init
  (global-flycheck-mode))

(use-package python
  :mode ("\\.py" . python-mode)
  :config
  (setq fill-column 79)
  (setq-default tab-width 4)
  (setq python-shell-interpreter "ipython"
		python-shell-interpreter-args "--simple-prompt -i")
  ;; (use-package elpy
  ;;   :commands elpy-enable
  ;;   :config
  ;;   (setq elpy-rpc-python-command "python3")
  ;;   (setq elpy-rpc-backend "jedi")
  ;;   (setq python-shell-interpreter "jupyter"
  ;; 	  python-shell-interpreter-args "console --simple-prompt"
  ;; 	  python-shell-prompt-detect-failure-warning nil)
  ;;   (add-to-list 'python-shell-completion-native-disabled-interpreters
  ;; 		 "jupyter")

  ;;   ;; use flycheck not flymake with elpy
  ;;   (when (require 'flycheck nil t)
  ;;     (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
  ;;     (add-hook 'elpy-mode-hook 'flycheck-mode))

  ;;   (add-hook 'elpy-mode-hook 'py-autopep8-enable-on-save))
  ;;   ;;fix IPython5 error
  ;;   (setenv "IPY_TEST_SIMPLE_PROMPT" "1")

  ;;   (elpy-enable)
  )

(use-package python-black)

(use-package ein) ;; jupyter support

(use-package yasnippet
  :init
  (yas-global-mode 1)
  :defer t)

(use-package vlf
  :config
  (require 'vlf-setup))

(use-package json-mode
  :mode "\\.json")

(use-package projectile
  :init (projectile-mode)
  :bind ("s-p" . projectile-command-map)
  :config
  (setq projectile-completion-system 'default)
  (setq projectile-switch-project-action 'projectile-dired)
  (setq projectile-mode-line
	'(:eval (if (file-remote-p default-directory)
		    " Prj[*remote*]"
		  (format " Prj[%s]" (projectile-project-name))))))


(use-package helm
  :ensure t
  :config
  (require 'helm-config)
  (global-unset-key (kbd "C-x c"))
  
  (define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action) ; rebind tab to run persistent action
  (define-key helm-map (kbd "C-i") 'helm-execute-persistent-action) ; make TAB work in terminal
  (global-set-key (kbd "M-x") 'helm-M-x)
  (global-set-key (kbd "C-x C-f") 'helm-find-files)
  (global-set-key (kbd "C-c h") 'helm-command-prefix)
  (global-set-key (kbd "C-c h s") 'helm-do-ag-this-file)
  ;; (setq helm-display-function 'helm-display-buffer-in-own-frame
  ;;       helm-display-buffer-reuse-frame t
  ;;       helm-use-undecorated-frame-option t)
  
  (use-package helm-ag)

  (use-package helm-projectile)

  (helm-projectile-on)
  (setq helm-split-window-inside-p          t ; open helm buffer inside current window, not occupy whole other window
      helm-move-to-line-cycle-in-source     t ; move to end or beginning of source when reaching top or bottom of source.
      helm-ff-search-library-in-sexp        t ; search for library in `require' and `declare-function' sexp.
      helm-scroll-amount                    8 ; scroll 8 lines other window using M-<next>/M-<prior>
      helm-ff-file-name-history-use-recentf t
      helm-echo-input-in-header-line t)

  (use-package helm-gtags)
  
  (defun spacemacs//helm-hide-minibuffer-maybe ()
  "Hide minibuffer in Helm session if we use the header line as input field."
  (when (with-helm-buffer helm-echo-input-in-header-line)
    (let ((ov (make-overlay (point-min) (point-max) nil nil t)))
      (overlay-put ov 'window (selected-window))
      (overlay-put ov 'face
                   (let ((bg-color (face-background 'default nil)))
                     `(:background ,bg-color :foreground ,bg-color)))
      (setq-local cursor-type nil))))

  (add-hook 'helm-minibuffer-set-up-hook
          'spacemacs//helm-hide-minibuffer-maybe)
  (setq helm-autoresize-max-height 0)
  (setq helm-autoresize-min-height 20)

  ;;C / C++ Stuff
  (setq
   helm-gtags-ignore-case t
   helm-gtags-auto-update t
   helm-gtags-use-input-at-cursor t
   helm-gtags-pulse-at-cursor t
   helm-gtags-prefix-key "\C-cg"
   helm-gtags-suggested-key-mapping t
   )

  (require 'helm-gtags)
  ;; Enable helm-gtags-mode
  (add-hook 'dired-mode-hook 'helm-gtags-mode)
  (add-hook 'eshell-mode-hook 'helm-gtags-mode)
  (add-hook 'c-mode-hook 'helm-gtags-mode)
  (add-hook 'c++-mode-hook 'helm-gtags-mode)
  (add-hook 'asm-mode-hook 'helm-gtags-mode)
;;  (add-hook 'jdee-mode 'helm-gtags-mode)

  (define-key helm-gtags-mode-map (kbd "C-c g a") 'helm-gtags-tags-in-this-function)
  (define-key helm-gtags-mode-map (kbd "C-c g f") 'helm-gtags-find-files)
  (define-key helm-gtags-mode-map (kbd "C-c g h") 'helm-gtags-find-tag-from-here)
  (define-key helm-gtags-mode-map (kbd "C-j") 'helm-gtags-select)
  (define-key helm-gtags-mode-map (kbd "M-.") 'helm-gtags-dwim)
  (define-key helm-gtags-mode-map (kbd "M-,") 'helm-gtags-pop-stack)
  (define-key helm-gtags-mode-map (kbd "s-[") 'helm-gtags-previous-history)
  (define-key helm-gtags-mode-map (kbd "s-]") 'helm-gtags-next-history)
  (define-key projectile-mode-map (kbd "s-p j") 'helm-gtags-find-tag)
  (setq-local imenu-create-index-function #'ggtags-build-imenu-index)

  (helm-autoresize-mode 1)

  (helm-mode +1)
  )

(use-package ace-window
  :bind ("C-x p" . ace-window))

(use-package function-args
  :interpreter ("c" . c-mode))

(use-package scala-mode
  :defer t
  :mode "\\.s\\(cala\\|bt\\)$")

;;Rust
(use-package rust-mode
  :config
  (add-hook 'rust-mode-hook #'racer-mode))

(use-package racer
  :config
  (add-hook 'racer-mode-hook #'eldoc-mode)
  (add-hook 'racer-mode-hook #'company-mode)
  (require 'rust-mode)
  (define-key rust-mode-map (kbd "TAB") #'company-indent-or-complete-common)
  (setq company-tooltip-align-annotations t)
  (setq racer-rust-src-path "/Users/yakir/.rustup/toolchains/stable-x86_64-apple-darwin/lib/rustlib/src/rust/src"))


(use-package yaml-mode)
;; (use-package ensime
;;   :defer t
;;   :after scala-mode
;;   :ensure t
;;   :pin melpa
;;   :config
;;   (setq ensime-search-interface 'helm)
;;   (setq ensime-startup-notification 'nil)
;;   (bind-key "M-." 'ensime-edit-definition-with-fallback ensime-mode-map)
;;   )

(use-package popup-imenu
  :commands popup-imenu
  :bind ("M-i" . popup-imenu))

;; (use-package jdee
;;   :mode ("\\.java\\'" . jdee-mode)
;;   :config
;;   (custom-set-variables
;;  '(jdee-server-dir "~/.emacs.d/jdee-server"))
;;   )

;; (use-package auto-complete
;;   :config
;;   (ac-config-default))

;;(use-package ox-reveal)

(use-package htmlize)

(use-package smartparens
  :ensure t
  :config
  (require 'smartparens-config)
  (sp-use-smartparens-bindings)
  (smartparens-strict-mode)

  (sp-pair "(" ")" :wrap "C-(") ;; how do people live without this?
  (sp-pair "[" "]" :wrap "M-[") ;; C-[ sends ESC
  (sp-pair "{" "}" :wrap "C-{")

  (bind-key "M-<backspace>" 'backward-kill-word smartparens-mode-map)
  (bind-key "C-<backspace>" 'sp-backward-unwrap-sexp smartparens-mode-map)
  ;; WORKAROUND https://github.com/Fuco1/smartparens/issues/543
  (bind-key "C-<left>" nil smartparens-mode-map)
  (bind-key "C-<right>" nil smartparens-mode-map)
  (bind-key "C-M-a" nil smartparens-mode-map)
  (bind-key "C-M-e" nil smartparens-mode-map)
  
  (bind-key "s-<delete>" 'sp-kill-sexp smartparens-mode-map)
  (bind-key "s-<backspace>" 'sp-backward-kill-sexp smartparens-mode-map)

  )

(use-package go-mode)

(use-package go-playground)

(use-package eyebrowse
  :config
  (add-to-list 'window-persistent-parameters '(window-side . writable))
  (add-to-list 'window-persistent-parameters '(window-slot . writable))
  )

;; (use-package org-brain
;;   :ensure t
;;   :init
;;   (setq org-brain-path "org/brain/")
;;   ;; For Evil users
;;   (with-eval-after-load 'evil
;;     (evil-set-initial-state 'org-brain-visualize-mode 'emacs))
;;   :config
;;   (setq org-id-track-globally t)
;;   (setq org-id-locations-file "~/.emacs.d/.org-id-locations")
;;   (push '("b" "Brain" plain (function org-brain-goto-end)
;;           "* %i%?" :empty-lines 1)
;;         org-capture-templates)
;;   (setq org-brain-visualize-default-choices 'all)
;;   (setq org-brain-title-max-length 20)
;;   (setq org-brain-include-file-entries nil
;;         org-brain-file-entries-use-title nil))

(use-package lsp-mode
  ;; set prefix for lsp-command-keymap (few alternatives - "C-l", "C-c l")
  :ensure t
  :config
  (setq lsp-keymap-prefix "s-l")
  (add-to-list 'exec-path "~/elixir-ls")
  (setq lsp-restart 'auto-restart)
  (setq lsp-enable-on-type-formatting nil)
;;  (push "[/\\\\]\\.*$" lsp-file-watch-ignored)
;;  (push "[/\\\\]\\_build$" lsp-file-watch-ignored)
  :hook (;; replace XXX-mode with concrete major-mode(e. g. python-mode)
     (elixir-mode . lsp)
	 (scala-mode  . lsp)
	 (lisp-moede   . lsp)
	 (js-mode . lsp)
     ;; if you want which-key integration
     (lsp-mode . lsp-enable-which-key-integration)
	 (lsp-mode . lsp-lens-mode)
	 (lsp-mode . smartparens-mode)
	 (lsp-mode . which-function-mode)
	 (lsp-mode . highlight-indent-guides-mode))
  :commands lsp
  )

(use-package lsp-pyright
  :ensure t
  :hook (python-mode . (lambda ()
                          (require 'lsp-pyright)
                          (lsp))))  ; or lsp-deferred

;; optionally
(use-package lsp-ui
  :ensure t
  :config
  (define-key lsp-ui-mode-map [remap xref-find-definitions] #'lsp-ui-peek-find-definitions)
  (define-key lsp-ui-mode-map [remap xref-find-references] #'lsp-ui-peek-find-references)
  (setq lsp-ui-sideline-enable t
	lsp-ui-doc-enable t
	lsp-ui-doc-position 'right
	lsp-ui-doc-include-signature t
	lsp-ui-flycheck-list-position 'bottom
	lsp-ui-sideline-show-code-actions t
	)
  :bind
    ([f9] . lsp-ui-flycheck-list)
  :commands lsp-ui-mode)

;;(use-package company-lsp :commands company-lsp)
;; if you are helm user
(use-package helm-lsp :commands helm-lsp-workspace-symbol)
;; if you are ivy user
(use-package lsp-ivy :commands lsp-ivy-workspace-symbol)


(use-package lsp-metals
  :config
  (setq lsp-metals-treeview-show-when-views-received nil)
  (setq lsp-metals-treeview-logging t)
  )

(use-package treemacs
  :bind (([f8] . treemacs)
  ([f6] . treemacs-add-and-display-current-project)
  ))

(use-package lsp-treemacs
  :config
  (lsp-treemacs-sync-mode 1)
  :bind
  ([f7] . lsp-treemacs-symbols))


;;optionally if you want to use debugger
(use-package posframe
  ;; Posframe is a pop-up tool that must be manually installed for dap-mode
  )

(use-package dap-mode
  :hook
  (lsp-mode . dap-mode)
  (lsp-mode . dap-ui-mode))

(use-package multiple-cursors
  :config
  (global-set-key (kbd "C->") 'mc/mark-next-like-this)
  (global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
  (global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this))


(use-package sbt-mode
  :commands sbt-start sbt-command
  :config
  ;; WORKAROUND: https://github.com/ensime/emacs-sbt-mode/issues/31
  ;; allows using SPACE when in the minibuffer
  (substitute-key-definition
   'minibuffer-complete-word
   'self-insert-command
   minibuffer-local-completion-map)
   ;; sbt-supershell kills sbt-mode:  https://github.com/hvesalai/emacs-sbt-mode/issues/152
   (setq sbt:program-options '("-Dsbt.supershell=false"))
   )

(use-package tide
  :ensure t
  :after (typescript-mode company flycheck)
  :hook ((typescript-mode . tide-setup)
         (typescript-mode . tide-hl-identifier-mode)
         (before-save . tide-format-before-save)))

(setq company-minimum-prefix-length 3
      company-idle-delay 0.1) ;; default is 0.2

;; General Configs
(eyebrowse-mode t)
(display-time-mode t)

(xterm-mouse-mode 1)
(global-set-key [mouse-4] 'scroll-down-line)
(global-set-key [mouse-5] 'scroll-up-line)

;; (global-set-key (kbd "M-.") 'helm-gtags-dwim)
;; (global-set-key (kbd "M-,") 'pop-tag-mark)

;;automatically save desktop
(desktop-save-mode 0)

(global-hl-line-mode t) ; highlights current line
(set-face-background 'hl-line "#7a7a3d")
;; (set-face-foreground 'highlight nil) ; Keep syntax higilghting
;; (set-face-attribute 'region nil :background "#666E92")

(global-linum-mode t) ;; enable line numbers globally
(menu-bar-mode -1)
(if (display-graphic-p)
    (progn
      (tool-bar-mode -1)
      (scroll-bar-mode -1)
      (when (eq system-type 'darwin)
      (set-face-attribute 'default nil :family "Source Code Pro for Powerline")
     (set-face-attribute 'default nil :height 170)
      (set-fontset-font t 'hangul (font-spec :name "Source Code Pro for Powerline"))
      )))

(setq gud-pdb-command-name "python3 -m pdb")

(add-hook 'prog-mode-hook
          (lambda ()
            (font-lock-add-keywords nil
                                    '(("\\<\\(FIXME\\|TODO\\|BUG\\):" 1 font-lock-warning-face t)))))

;; use shift for moving between windows
(when (fboundp 'windmove-default-keybindings)
  (windmove-default-keybindings))

;; change history shortcuts
(global-set-key (kbd "M-<up>") 'comint-previous-input)
(global-set-key (kbd "M-<down>") 'comint-next-input)
(global-set-key (kbd "M-*") 'pop-tag-mark)
(global-set-key (kbd "C-x t +") 'text-scale-increase)
(global-set-key (kbd "C-x t -") 'text-scale-decrease)
(global-set-key (kbd "<C-up>") 'shrink-window)
(global-set-key (kbd "<C-down>") 'enlarge-window)
(global-set-key (kbd "<C-left>") 'shrink-window-horizontally)
(global-set-key (kbd "<C-right>") 'enlarge-window-horizontally)
(global-set-key (kbd "<C-tab>") 'company-complete)
(global-set-key (kbd "C-M-a") 'beginning-of-defun)
(global-set-key (kbd "C-M-e") 'end-of-defun)

;; Comments
(global-set-key (kbd "M-/") 'comment-or-uncomment-region)

;;Environment variables for lang
(setenv "LC_ALL" "en_US.UTF-8")
(setenv "LANG" "en_US.UTF-8")

;;Use java8
(setenv "JAVA_HOME" (string-trim (shell-command-to-string "/usr/libexec/java_home -v 1.8")))

;;gtags conf
(setenv "GTAGSCONF" "/usr/local/share/gtags/gtags.conf")
(setenv "GTAGSLABEL" "ctags")

;;paren mode
(show-paren-mode 1)

;;Start server
(server-start)

;;Allow in place deletion / replace
(delete-selection-mode 1)

;;Should accelerate TRAMP
(setq remote-file-name-inhibit-cache nil)
(setq vc-ignore-dir-regexp
      (format "%s\\|%s"
                    vc-ignore-dir-regexp
                    tramp-file-name-regexp))
(setq tramp-verbose 1)

;;Ensime edit definition with fallback
;; (defun ensime-edit-definition-with-fallback ()
;;   "Variant of `ensime-edit-definition' with ctags if ENSIME is not available."
;;   (interactive)
;;   (unless (and (ensime-connection-or-nil)
;;                (ensime-edit-definition))
;;     (projectile-find-tag)))

(defun strike-through-for-org-mode ()
  "Strike through for org mode."
      (interactive)
      (beginning-of-line)
      (save-excursion
        (if (string-prefix-p "*" (thing-at-point 'line t))
            (progn
              (setq go_char (string-match "[ ]" (thing-at-point 'line t)))
              (forward-char (+ go_char 1))
              (insert "+")
              (end-of-line)
              (insert "+")
              )
          (if (string-match "[^ ]" (thing-at-point 'line t))
              (progn
                (setq go_char (string-match "[^ ]" (thing-at-point 'line t)))
                (forward-char (+ go_char 2))
                (insert "+")
                (end-of-line)
                (insert "+")
                )
            (message "[-] Not Proper Position!")
            )
          )
        )
      )

(defun strike-through-for-org-mode-undo ()
  "Strike through for org mode undo."
  (interactive)
  (beginning-of-line)
  (save-excursion
    (if (string-match "[+]" (thing-at-point 'line t))
        (progn
          (setq go_char (string-match "[+]" (thing-at-point 'line t)))
          (forward-char go_char)
          (delete-char 1)
          (end-of-line)
          (delete-char -1)
          )
      (message "[-] Not Proper Position!")
      )
    )
    )

;; EMACS STUFF BELOW
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(helm-buffer-max-length 40)
 '(jdee-server-dir "~/.emacs.d/jdee-server")
 '(org-agenda-files
   '("~/org/identity/Identity.org" "~/org/identity/idsyncinsnapshot.org" "~/Library/Mobile Documents/iCloud~com~appsonthemove~beorg/Documents/org/superset.org" "~/org/MediaAppFramework.org" "~/org/Itsik.org" "~/org/Arun.org" "~/Library/Mobile Documents/iCloud~com~appsonthemove~beorg/Documents/org/Shachaf.org" "~/Library/Mobile Documents/iCloud~com~appsonthemove~beorg/Documents/org/Alon.org" "~/org/Yaar.org" "~/org/Gregory.org" "~/org/ExtendedLeadership.org" "~/Library/Mobile Documents/iCloud~com~appsonthemove~beorg/Documents/org/Idan.org" "~/Library/Mobile Documents/iCloud~com~appsonthemove~beorg/Documents/org/syndication.org" "~/Library/Mobile Documents/iCloud~com~appsonthemove~beorg/Documents/org/Arik.org" "~/Library/Mobile Documents/iCloud~com~appsonthemove~beorg/Documents/org/Offsite2019.org" "~/Library/Mobile Documents/iCloud~com~appsonthemove~beorg/Documents/org/Vlad.org" "~/Library/Mobile Documents/iCloud~com~appsonthemove~beorg/Documents/org/globaltodo.org"))
 '(org-export-backends '(ascii html icalendar latex md))
 '(package-selected-packages
   '(yaml prettier org-roam yaml-mode tide centaur-tabs python-black yapfify highlight-indent-guides lsp-ui treemacs lsp-metals lsp-mode posframe ein lsp-treemacs darkroom multiple-cursors dash org-plus-contrib imenu-list org-brain eyebrowse magit racer alchemist doom-modeline company org-trello web-beautify etags-select htmlize auto-package-update org ox-reveal terraform-mode php-mode ob-shell ggtags helm-ag doremi-cmd py-autopep8 helm-gtags helm-projectile popup-imenu scala-mode function-args doremi ace-window helm projectile json-mode neotree vlf yasnippet flycheck avy exec-path-from-shell rainbow-delimiters moe-theme powerline all-the-icons org-bullets which-key try use-package)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(put 'narrow-to-region 'disabled nil)
